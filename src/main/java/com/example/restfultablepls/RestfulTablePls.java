package com.example.restfultablepls;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.BaseMacro;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.user.User;
import org.apache.log4j.Logger;

import java.util.Map;

import static org.apache.log4j.Logger.getLogger;

public class RestfulTablePls extends BaseMacro implements Macro {

    private final Logger logger = getLogger(RestfulTablePls.class);

    private static final String TEMPLATE = "templates/restfultable-pls.vm";

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {
        try {
            return execute(parameters, body, ((RenderContext) null));
        } catch (MacroException e) {
            throw new MacroExecutionException(e);
        }
    }

    @Override
    public String execute(Map params, String body, RenderContext renderContext) throws MacroException {

        Map<String, Object> context = MacroUtils.defaultVelocityContext();

        User user = AuthenticatedUserThreadLocal.getUser();
        if (user != null) {
            context.put("userFullName", user.getFullName());
            context.put("userEmail", user.getEmail());
        }

        return VelocityUtils.getRenderedTemplate(TEMPLATE, context);
    }

    @Override
    public boolean isInline() {
        return false;
    }

    @Override
    public boolean hasBody() {
        return false;
    }

    @Override
    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }

}
